package com.kyjg.fitnessassistmanager.repository;

import com.kyjg.fitnessassistmanager.entity.FitnessHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface FitnessHistoryRepository extends JpaRepository<FitnessHistory, Long> {
    List<FitnessHistory> findAllByIdOrderByIdDesc(long id);
    List<FitnessHistory> findAllByUserInfo_IdAndTodayDate(long userInfoId, LocalDate searchDate);
}
