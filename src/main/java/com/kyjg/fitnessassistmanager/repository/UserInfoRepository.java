package com.kyjg.fitnessassistmanager.repository;

import com.kyjg.fitnessassistmanager.entity.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserInfoRepository extends JpaRepository<UserInfo, Long> {
    List<UserInfo> findAllByIdOrderByIdDesc(long id);
}
