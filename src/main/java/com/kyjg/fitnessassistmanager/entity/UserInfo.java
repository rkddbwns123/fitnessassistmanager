package com.kyjg.fitnessassistmanager.entity;

import com.kyjg.fitnessassistmanager.enums.FitnessAbility;
import com.kyjg.fitnessassistmanager.enums.Gender;
import com.kyjg.fitnessassistmanager.interfaces.CommonModelBuilder;
import com.kyjg.fitnessassistmanager.model.userinfo.UserInfoRequest;
import com.kyjg.fitnessassistmanager.model.userinfo.UserInfoUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UserInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 20)
    private String userName;
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private Gender gender;
    @Column(nullable = false)
    private Double userHeight;
    @Column(nullable = false)
    private Double userWeight;
    @Column(nullable = false)
    private LocalDateTime lastConnect;
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private FitnessAbility fitnessAbility;
    @Column(nullable = false)
    private Integer dailyGoalMinute;

    public void putUserInfo(UserInfoUpdateRequest request) {
        this.userWeight = request.getUserWeight();
        this.fitnessAbility = request.getFitnessAbility();
        this.dailyGoalMinute = request.getDailyGoalMinute();
    }
    public void putLastConnect() {
        this.lastConnect = LocalDateTime.now();
    }

    private UserInfo(UserInfoBuilder builder) {
        this.userName = builder.userName;
        this.gender = builder.gender;
        this.userHeight = builder.userHeight;
        this.userWeight = builder.userWeight;
        this.lastConnect = builder.lastConnect;
        this.fitnessAbility = builder.fitnessAbility;
        this.dailyGoalMinute = builder.dailyGoalMinute;

    }

    public static class UserInfoBuilder implements CommonModelBuilder<UserInfo> {

        private final String userName;
        private final Gender gender;
        private final Double userHeight;
        private final Double userWeight;
        private final LocalDateTime lastConnect;
        private final FitnessAbility fitnessAbility;
        private final Integer dailyGoalMinute;

        public UserInfoBuilder(UserInfoRequest request) {
            this.userName = request.getUserName();
            this.gender = request.getGender();
            this.userHeight = request.getUserHeight();
            this.userWeight = request.getUserWeight();
            this.lastConnect = LocalDateTime.now();
            this.fitnessAbility = request.getFitnessAbility();
            this.dailyGoalMinute = request.getDailyGoalMinute();
        }

        @Override
        public UserInfo build() {
            return new UserInfo(this);
        }
    }
}