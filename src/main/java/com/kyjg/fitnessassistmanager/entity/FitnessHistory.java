package com.kyjg.fitnessassistmanager.entity;

import com.kyjg.fitnessassistmanager.enums.ExercisePart;
import com.kyjg.fitnessassistmanager.interfaces.CommonModelBuilder;
import com.kyjg.fitnessassistmanager.model.fitness.FitnessHistoryRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class FitnessHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userInfoId", nullable = false)
    private UserInfo userInfo;
    @Column(nullable = false)
    private LocalDate todayDate;
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private ExercisePart exercisePart;
    @Column(nullable = false, length = 30)
    private String exerciseName;
    @Column(nullable = false)
    private Integer exerciseWeight;
    @Column(nullable = false)
    private Integer exerciseSetValue;
    @Column(nullable = false)
    private Integer exerciseValue;
    @Column(nullable = false)
    private LocalTime startFitness;
    @Column(nullable = false)
    private LocalTime finishFitness;
    @Column(nullable = false)
    private Boolean isFinished;

    public void putFinishFitness() {
        this.finishFitness = LocalTime.now();
        this.isFinished = true;
    }

    private FitnessHistory(FitnessHistoryBuilder builder) {
        this.userInfo = builder.userInfo;
        this.todayDate = builder.todayDate;
        this.exercisePart = builder.exercisePart;
        this.exerciseName = builder.exerciseName;
        this.exerciseWeight = builder.exerciseWeight;
        this.exerciseSetValue = builder.exerciseSetValue;
        this.exerciseValue = builder.exerciseValue;
        this.startFitness = builder.startFitness;
        this.finishFitness = builder.finishFitness;
        this.isFinished = builder.isFinished;
    }

    public static class FitnessHistoryBuilder implements CommonModelBuilder<FitnessHistory> {

        private final UserInfo userInfo;
        private final LocalDate todayDate;
        private final ExercisePart exercisePart;
        private final String exerciseName;
        private final Integer exerciseWeight;
        private final Integer exerciseSetValue;
        private final Integer exerciseValue;
        private final LocalTime startFitness;
        private final LocalTime finishFitness;
        private final Boolean isFinished;


        public FitnessHistoryBuilder(UserInfo userInfo, FitnessHistoryRequest request) {
            this.userInfo = userInfo;
            this.todayDate = LocalDate.now();
            this.exercisePart = request.getExercisePart();
            this.exerciseName = request.getExerciseName();
            this.exerciseWeight = request.getExerciseWeight();
            this.exerciseSetValue = request.getExerciseSetValue();
            this.exerciseValue = request.getExerciseValue();
            this.startFitness = LocalTime.now();
            this.finishFitness = this.startFitness;
            this.isFinished = false;
        }

        @Override
        public FitnessHistory build() {
            return new FitnessHistory(this);
        }
    }
}
