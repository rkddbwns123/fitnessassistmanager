package com.kyjg.fitnessassistmanager.controller;

import com.kyjg.fitnessassistmanager.entity.UserInfo;
import com.kyjg.fitnessassistmanager.model.CommonResult;
import com.kyjg.fitnessassistmanager.model.fitness.FitnessHistoryItem;
import com.kyjg.fitnessassistmanager.model.fitness.FitnessHistoryRequest;
import com.kyjg.fitnessassistmanager.model.ListResult;
import com.kyjg.fitnessassistmanager.service.FitnessHistoryService;
import com.kyjg.fitnessassistmanager.service.ResponseService;
import com.kyjg.fitnessassistmanager.service.UserInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
@Api(tags = "운동 기록 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/fitness/history")
public class FitnessHistoryController {
    private final FitnessHistoryService fitnessHistoryService;

    private final UserInfoService userInfoService;
    @ApiOperation(value = "기록 등록")
    @PostMapping("/data/id/{id}")
    public CommonResult setFitnessHistory(@PathVariable long id, @RequestBody @Valid FitnessHistoryRequest request) {
        UserInfo userInfo = userInfoService.getData(id);
        fitnessHistoryService.setFitnessHistory(userInfo, request);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "기록 리스트")
    @GetMapping("/history/")
    public ListResult<FitnessHistoryItem> getHistories() {
        return ResponseService.getListResult(fitnessHistoryService.getHistories(), true);
    }
    @ApiOperation(value = "운동 종료")
    @PutMapping("/finish/id/{id}")
    public CommonResult putFinishFitness(@PathVariable long id) {
        fitnessHistoryService.putFinishFitness(id);

        return ResponseService.getSuccessResult();
    }
}
