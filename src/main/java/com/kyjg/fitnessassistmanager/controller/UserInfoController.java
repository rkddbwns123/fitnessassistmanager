package com.kyjg.fitnessassistmanager.controller;

import com.kyjg.fitnessassistmanager.model.*;
import com.kyjg.fitnessassistmanager.model.userinfo.UserInfoItem;
import com.kyjg.fitnessassistmanager.model.userinfo.UserInfoRequest;
import com.kyjg.fitnessassistmanager.model.userinfo.UserInfoResponse;
import com.kyjg.fitnessassistmanager.model.userinfo.UserInfoUpdateRequest;
import com.kyjg.fitnessassistmanager.service.ResponseService;
import com.kyjg.fitnessassistmanager.service.UserInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
@Api(tags = "회원 정보")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/user-info")
public class UserInfoController {
    private final UserInfoService userInfoService;
    @ApiOperation(value = "회원 정보 등록")
    @PostMapping("/data")
    public CommonResult setUserInfo(@RequestBody @Valid UserInfoRequest request) {
        userInfoService.setUserInfo(request);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "회원 정보 리스트")
    @GetMapping("/all")
    public ListResult<UserInfoItem> getUserInfo() {

        return ResponseService.getListResult(userInfoService.getUserInfo(), true);
    }
    @ApiOperation(value = "회원 상세 정보")
    @GetMapping("/detail/{id}")
    public SingleResult<UserInfoResponse> getUserInfoById(@PathVariable long id) {
        return ResponseService.getSingleResult(userInfoService.getUserInfoById(id));
    }

    @ApiOperation(value = "회원 정보 수정")
    @PutMapping("/update/id/{id}")
    public CommonResult putUserInfo(@PathVariable long id, @RequestBody @Valid UserInfoUpdateRequest request) {
        userInfoService.putUserInfo(id, request);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "마지막 방문일 수정")
    @PutMapping("/date-update/id/{id}")
    public CommonResult putLastConnect(@PathVariable long id) {
        userInfoService.putLastConnect(id);

        return ResponseService.getSuccessResult();
    }
}
