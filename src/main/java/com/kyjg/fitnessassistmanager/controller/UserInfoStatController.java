package com.kyjg.fitnessassistmanager.controller;

import com.kyjg.fitnessassistmanager.model.SingleResult;
import com.kyjg.fitnessassistmanager.model.userinfo.UserInfoStatResponse;
import com.kyjg.fitnessassistmanager.service.ResponseService;
import com.kyjg.fitnessassistmanager.service.UserInfoStatService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@Api(tags = "등록된 회원 전체 통계")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/stat")
public class UserInfoStatController {
    private final UserInfoStatService userInfoStatService;
    @ApiOperation(value = "회원 전체 통계 불러오기")
    @GetMapping("/user")
    public SingleResult<UserInfoStatResponse> getStat() {
        return ResponseService.getSingleResult(userInfoStatService.getUserStat());
    }
}
