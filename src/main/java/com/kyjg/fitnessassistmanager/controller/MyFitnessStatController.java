package com.kyjg.fitnessassistmanager.controller;

import com.kyjg.fitnessassistmanager.model.SingleResult;
import com.kyjg.fitnessassistmanager.model.fitness.MyFitnessGoalPercentResponse;
import com.kyjg.fitnessassistmanager.model.fitness.MyFitnessResponse;
import com.kyjg.fitnessassistmanager.service.MyFitnessStatService;
import com.kyjg.fitnessassistmanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@Api(tags = "회원 별 기록")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/my/fitness")
public class MyFitnessStatController {
    private final MyFitnessStatService myFitnessStatService;
    @ApiOperation("회원 한 명의 오늘의 기록")
    @GetMapping("/stat/{userInfoId}")
    public SingleResult<MyFitnessResponse> getMyFitnessStat(@PathVariable long userInfoId) {
        return ResponseService.getSingleResult(myFitnessStatService.getMyFitnessStat(userInfoId));
    }
    @ApiOperation("오늘의 목표치 달성 여부(%)")
    @GetMapping("/goal/percent/{userInfoId}")
    public SingleResult<MyFitnessGoalPercentResponse> getGoalPercent(@PathVariable long userInfoId) {
        return ResponseService.getSingleResult(myFitnessStatService.getGoalPercent(userInfoId));
    }
}
