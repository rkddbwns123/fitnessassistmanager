package com.kyjg.fitnessassistmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FitnessAssistManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FitnessAssistManagerApplication.class, args);
	}

}
