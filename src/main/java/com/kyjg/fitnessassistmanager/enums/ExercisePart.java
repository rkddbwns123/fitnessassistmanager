package com.kyjg.fitnessassistmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ExercisePart {
    CHEST("가슴"),
    TRICEPS("삼두"),
    BACK("등"),
    BICEPS("이두"),
    LOWER_BODY("하체"),
    SHOULDERS("어깨"),
    ETC("기타");

    private final String partName;
}
