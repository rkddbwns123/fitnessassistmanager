package com.kyjg.fitnessassistmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FitnessAbility {
    BEGINNER("초급자"),
    INTERMEDIATE("중급자"),
    SENIOR("상급자");

    private final String abilityName;
}
