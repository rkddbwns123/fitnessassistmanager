package com.kyjg.fitnessassistmanager.service;

import com.kyjg.fitnessassistmanager.entity.UserInfo;
import com.kyjg.fitnessassistmanager.exception.CMissingDataException;
import com.kyjg.fitnessassistmanager.model.ListResult;
import com.kyjg.fitnessassistmanager.model.userinfo.UserInfoItem;
import com.kyjg.fitnessassistmanager.model.userinfo.UserInfoRequest;
import com.kyjg.fitnessassistmanager.model.userinfo.UserInfoResponse;
import com.kyjg.fitnessassistmanager.model.userinfo.UserInfoUpdateRequest;
import com.kyjg.fitnessassistmanager.repository.UserInfoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserInfoService {
    private final UserInfoRepository userInfoRepository;

    public void setUserInfo(UserInfoRequest request) {
        UserInfo addData = new UserInfo.UserInfoBuilder(request).build();

        userInfoRepository.save(addData);
    }
    public UserInfo getData(long id) {
        return userInfoRepository.findById(id).orElseThrow(CMissingDataException::new);
    }
    public ListResult<UserInfoItem> getUserInfo() {
        List<UserInfo> originList = userInfoRepository.findAll();

        List<UserInfoItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new UserInfoItem.UserInfoItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }
    public UserInfoResponse getUserInfoById(long id) {
        UserInfo originData = userInfoRepository.findById(id).orElseThrow(CMissingDataException::new);

        return new UserInfoResponse.UserInfoResponseBuilder(originData).build();
    }

    public void putUserInfo(long id, UserInfoUpdateRequest request) {
        UserInfo originData = userInfoRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putUserInfo(request);

        userInfoRepository.save(originData);
    }
    public void putLastConnect(long id) {
        UserInfo originData = userInfoRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putLastConnect();

        userInfoRepository.save(originData);
    }
}
