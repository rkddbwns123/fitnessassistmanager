package com.kyjg.fitnessassistmanager.service;

import com.kyjg.fitnessassistmanager.entity.FitnessHistory;
import com.kyjg.fitnessassistmanager.exception.CMissingDataException;
import com.kyjg.fitnessassistmanager.model.fitness.MyFitnessGoalPercentResponse;
import com.kyjg.fitnessassistmanager.model.fitness.MyFitnessItem;
import com.kyjg.fitnessassistmanager.model.fitness.MyFitnessResponse;
import com.kyjg.fitnessassistmanager.repository.FitnessHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class MyFitnessStatService {
    private final FitnessHistoryRepository fitnessHistoryRepository;

    public MyFitnessResponse getMyFitnessStat(long userInfoId) {
        List<FitnessHistory> originList = fitnessHistoryRepository.findAllByUserInfo_IdAndTodayDate(userInfoId, LocalDate.now());

        if (originList.isEmpty()) throw new CMissingDataException();

        long totalRunningTime = 0;
        List<String> partNames = new LinkedList<>();
        List<MyFitnessItem> fitnessItems = new LinkedList<>();

        for (FitnessHistory item : originList) {
            Duration duration = Duration.between(item.getStartFitness(), item.getFinishFitness());
            long seconds = duration.getSeconds();
            totalRunningTime += seconds / 60;

            partNames.add(item.getExercisePart().getPartName());

            MyFitnessItem addItem = new MyFitnessItem.MyFitnessItemBuilder(
                    item.getExercisePart().getPartName(),
                    seconds / 60,
                    item.getExerciseWeight(),
                    item.getExerciseValue() * item.getExerciseSetValue()).build();
            fitnessItems.add(addItem);
        }
        Set<String> partNamesResult = new HashSet<>(partNames);
        String partNamesResultName = String.join(" / ", partNamesResult);

        return new MyFitnessResponse.MyFitnessResponseBuilder(totalRunningTime + "분", partNamesResultName,fitnessItems).build();
    }

    public MyFitnessGoalPercentResponse getGoalPercent(long userInfoId) {
        List<FitnessHistory> originList = fitnessHistoryRepository.findAllByUserInfo_IdAndTodayDate(userInfoId, LocalDate.now());

        if (originList.isEmpty()) throw new CMissingDataException();

        List<String> partNames = new LinkedList<>();
        double totalGoalPercent = 0;

        for (FitnessHistory item : originList) {

            partNames.add(item.getExercisePart().getPartName());

            Duration duration = Duration.between(item.getStartFitness(), item.getFinishFitness());
            double minute = duration.getSeconds() / 60;

            totalGoalPercent += minute / item.getUserInfo().getDailyGoalMinute() * 100;
        }
        String partNamesResult = String.join(" / ", partNames);

        return new MyFitnessGoalPercentResponse.MyFitnessGoalPercentResponseBuilder(partNamesResult, totalGoalPercent + "%").build();
    }

}
