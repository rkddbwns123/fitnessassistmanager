package com.kyjg.fitnessassistmanager.service;

import com.kyjg.fitnessassistmanager.entity.FitnessHistory;
import com.kyjg.fitnessassistmanager.entity.UserInfo;
import com.kyjg.fitnessassistmanager.exception.CMissingDataException;
import com.kyjg.fitnessassistmanager.model.fitness.FitnessHistoryItem;
import com.kyjg.fitnessassistmanager.model.fitness.FitnessHistoryRequest;
import com.kyjg.fitnessassistmanager.model.ListResult;
import com.kyjg.fitnessassistmanager.repository.FitnessHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FitnessHistoryService {
    private final FitnessHistoryRepository fitnessHistoryRepository;

    public void setFitnessHistory(UserInfo userInfo, FitnessHistoryRequest request) {
        FitnessHistory addData = new FitnessHistory.FitnessHistoryBuilder(userInfo, request).build();

        fitnessHistoryRepository.save(addData);
    }

    public ListResult<FitnessHistoryItem> getHistories() {
        List<FitnessHistory> originList = fitnessHistoryRepository.findAll();

        List<FitnessHistoryItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new FitnessHistoryItem.FitnessHistoryItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }
    public void putFinishFitness(long id) {
        FitnessHistory originData = fitnessHistoryRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putFinishFitness();

        fitnessHistoryRepository.save(originData);
    }
}
