package com.kyjg.fitnessassistmanager.service;

import com.kyjg.fitnessassistmanager.entity.UserInfo;
import com.kyjg.fitnessassistmanager.enums.FitnessAbility;
import com.kyjg.fitnessassistmanager.model.userinfo.UserInfoStatResponse;
import com.kyjg.fitnessassistmanager.repository.UserInfoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserInfoStatService {
    private final UserInfoRepository userInfoRepository;

    public UserInfoStatResponse getUserStat() {
        List<UserInfo> originList = userInfoRepository.findAll();

        long totalUserCount = originList.size();
        double userHeight = 0;
        double userWeight = 0;
        long beginnerCount = 0;
        long intermediateCount = 0;
        long seniorCount = 0;

        for(UserInfo item : originList) {
            userHeight += item.getUserHeight();
            userWeight += item.getUserWeight();
            if (item.getFitnessAbility().equals(FitnessAbility.BEGINNER)) {
                beginnerCount += 1;
            } else if (item.getFitnessAbility().equals(FitnessAbility.INTERMEDIATE)) {
                intermediateCount += 1;
            } else if (item.getFitnessAbility().equals(FitnessAbility.SENIOR)) {
                seniorCount += 1;
            }
        }

        return new UserInfoStatResponse.FitnessStatResponseBuilder(
                totalUserCount,
                userHeight / totalUserCount,
                userWeight / totalUserCount,
                beginnerCount,
                intermediateCount,
                seniorCount
        ).build();
    }

}
