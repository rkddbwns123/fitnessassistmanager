package com.kyjg.fitnessassistmanager.model.fitness;

import com.kyjg.fitnessassistmanager.entity.FitnessHistory;
import com.kyjg.fitnessassistmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class FitnessHistoryItem {
    @NotNull
    private Long historyId;
    private String userName;
    private String todayExercisePart;
    private String exerciseName;
    private String exerciseValue;
    private LocalDate todayDate;
    private LocalTime startFitness;
    private LocalTime finishFitness;
    private String isFinished;

    private FitnessHistoryItem(FitnessHistoryItemBuilder builder) {
        this.historyId = builder.historyId;
        this.userName = builder.userName;
        this.todayExercisePart = builder.todayExercisePart;
        this.exerciseName = builder.exerciseName;
        this.exerciseValue = builder.exerciseValue;
        this.todayDate = builder.todayDate;
        this.startFitness = builder.startFitness;
        this.finishFitness = builder.finishFitness;
        this.isFinished = builder.isFinished;

    }

    public static class FitnessHistoryItemBuilder implements CommonModelBuilder<FitnessHistoryItem> {

        private final Long historyId;
        private final String userName;
        private final String todayExercisePart;
        private final String exerciseName;
        private final String exerciseValue;
        private final LocalDate todayDate;
        private final LocalTime startFitness;
        private final LocalTime finishFitness;
        private final String isFinished;

        public FitnessHistoryItemBuilder(FitnessHistory history) {
            this.historyId = history.getId();
            this.userName = history.getUserInfo().getUserName();
            this.todayExercisePart = history.getExercisePart().getPartName();
            this.exerciseName = history.getExerciseName();
            this.exerciseValue = history.getExerciseSetValue() + "세트 / "
                    + history.getExerciseValue() + "회 / " + history.getExerciseWeight() + "kg";
            this.todayDate = history.getTodayDate();
            this.startFitness = history.getStartFitness();
            this.finishFitness = this.startFitness;
            this.isFinished = history.getIsFinished() ? "운동 끝" : "운동 중";
            }

        @Override
        public FitnessHistoryItem build() {
            return new FitnessHistoryItem(this);
        }
    }

}
