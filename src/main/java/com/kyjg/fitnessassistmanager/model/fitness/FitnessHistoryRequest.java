package com.kyjg.fitnessassistmanager.model.fitness;

import com.kyjg.fitnessassistmanager.enums.ExercisePart;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
@Getter
@Setter
public class FitnessHistoryRequest {
    @ApiModelProperty(value = "운동 부위", required = true)
    @Enumerated(value = EnumType.STRING)
    @NotNull
    private ExercisePart exercisePart;
    @ApiModelProperty(value = "운동 이름 (1~30자)", required = true)
    @NotNull
    @Length(min = 1, max = 30)
    private String exerciseName;
    @ApiModelProperty(value = "운동 중량(1~500)", required = true)
    @NotNull
    @Min(value = 1)
    @Max(value = 500)
    private Integer exerciseWeight;
    @ApiModelProperty(value = "운동 세트 수(1~50)", required = true)
    @NotNull
    @Min(value = 1)
    @Max(value = 50)
    private Integer exerciseSetValue;
    @ApiModelProperty(value = "세트 별 횟수(1~100)", required = true)
    @NotNull
    @Min(value = 1)
    @Max(value = 100)
    private Integer exerciseValue;
}
