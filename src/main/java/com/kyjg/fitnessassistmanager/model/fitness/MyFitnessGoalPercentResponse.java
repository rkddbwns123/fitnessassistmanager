package com.kyjg.fitnessassistmanager.model.fitness;

import com.kyjg.fitnessassistmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MyFitnessGoalPercentResponse {
    private String partName;
    private LocalDate todayDate;
    private String totalGoalPercent;

    private MyFitnessGoalPercentResponse(MyFitnessGoalPercentResponseBuilder builder) {
        this.partName = builder.partName;
        this.todayDate = builder.todayDate;
        this.totalGoalPercent = builder.totalGoalPercent;

    }
    public static class MyFitnessGoalPercentResponseBuilder implements CommonModelBuilder<MyFitnessGoalPercentResponse> {
        private final String partName;
        private final LocalDate todayDate;
        private final String totalGoalPercent;

        public MyFitnessGoalPercentResponseBuilder(String partName, String totalGoalPercent) {
            this.partName = partName;
            this.todayDate = LocalDate.now();
            this.totalGoalPercent = totalGoalPercent;
        }

        @Override
        public MyFitnessGoalPercentResponse build() {
            return new MyFitnessGoalPercentResponse(this);
        }
    }
}
