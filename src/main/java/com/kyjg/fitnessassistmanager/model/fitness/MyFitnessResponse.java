package com.kyjg.fitnessassistmanager.model.fitness;

import com.kyjg.fitnessassistmanager.entity.UserInfo;
import com.kyjg.fitnessassistmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MyFitnessResponse {
    private String totalRunningTimeName;
    private String todayParts;
    private List<MyFitnessItem> fitnessItems;

    private MyFitnessResponse(MyFitnessResponseBuilder builder) {
        this.totalRunningTimeName = builder.totalRunningTimeName;
        this.todayParts = builder.todayParts;
        this.fitnessItems = builder.fitnessItems;

    }

    public static class MyFitnessResponseBuilder implements CommonModelBuilder<MyFitnessResponse> {
        private final String totalRunningTimeName;
        private final String todayParts;
        private final List<MyFitnessItem> fitnessItems;

        public MyFitnessResponseBuilder(String totalRunningTimeName, String todayParts, List<MyFitnessItem> fitnessItems) {
            this.totalRunningTimeName = totalRunningTimeName;
            this.todayParts = todayParts;
            this.fitnessItems = fitnessItems;
        }

        @Override
        public MyFitnessResponse build() {
            return new MyFitnessResponse(this);
        }
    }
}
