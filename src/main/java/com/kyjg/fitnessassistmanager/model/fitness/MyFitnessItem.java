package com.kyjg.fitnessassistmanager.model.fitness;

import com.kyjg.fitnessassistmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MyFitnessItem {
    private String exercisePartName;
    private Long runningTime;
    private Integer exerciseWeight;
    private Integer totalCount;

    private MyFitnessItem(MyFitnessItemBuilder builder) {
        this.exercisePartName = builder.exercisePartName;
        this.runningTime = builder.runningTime;
        this.exerciseWeight = builder.exerciseWeight;
        this.totalCount = builder.totalCount;

    }

    public static class MyFitnessItemBuilder implements CommonModelBuilder<MyFitnessItem> {

        private final String exercisePartName;
        private final Long runningTime;
        private final Integer exerciseWeight;
        private final Integer totalCount;

        public MyFitnessItemBuilder(String exercisePartName, Long runningTime, Integer exerciseWeight, Integer totalCount) {
            this.exercisePartName = exercisePartName;
            this.runningTime = runningTime;
            this.exerciseWeight = exerciseWeight;
            this.totalCount = totalCount;
        }

        @Override
        public MyFitnessItem build() {
            return new MyFitnessItem(this);
        }
    }
}
