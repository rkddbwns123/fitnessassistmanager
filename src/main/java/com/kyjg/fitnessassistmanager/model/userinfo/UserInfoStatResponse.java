package com.kyjg.fitnessassistmanager.model.userinfo;

import com.kyjg.fitnessassistmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UserInfoStatResponse {
    private Long totalUserCount;
    private Double averageUserHeight;
    private Double averageUserWeight;
    private Long beginnerUserCount;
    private Long intermediateUserCount;
    private Long seniorUserCount;

    private UserInfoStatResponse(FitnessStatResponseBuilder builder) {
        this.totalUserCount = builder.totalUserCount;
        this.averageUserHeight = builder.averageUserHeight;
        this.averageUserWeight = builder.averageUserWeight;
        this.beginnerUserCount = builder.beginnerUserCount;
        this.intermediateUserCount = builder.intermediateUserCount;
        this.seniorUserCount = builder.seniorUserCount;

    }
    public static class FitnessStatResponseBuilder implements CommonModelBuilder<UserInfoStatResponse> {

        private final Long totalUserCount;
        private final Double averageUserHeight;
        private final Double averageUserWeight;
        private final Long beginnerUserCount;
        private final Long intermediateUserCount;
        private final Long seniorUserCount;

        public FitnessStatResponseBuilder(
                Long totalUserCount, Double averageUserHeight,
                Double averageUserWeight, Long beginnerUserCount,
                Long intermediateUserCount, Long seniorUserCount) {
            this.totalUserCount = totalUserCount;
            this.averageUserHeight = averageUserHeight;
            this.averageUserWeight = averageUserWeight;
            this.beginnerUserCount = beginnerUserCount;
            this.intermediateUserCount = intermediateUserCount;
            this.seniorUserCount = seniorUserCount;

        }
        @Override
        public UserInfoStatResponse build() {
            return new UserInfoStatResponse(this);
        }
    }
}
