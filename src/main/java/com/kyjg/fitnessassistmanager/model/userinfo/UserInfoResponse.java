package com.kyjg.fitnessassistmanager.model.userinfo;

import com.kyjg.fitnessassistmanager.entity.UserInfo;
import com.kyjg.fitnessassistmanager.enums.FitnessAbility;
import com.kyjg.fitnessassistmanager.enums.Gender;
import com.kyjg.fitnessassistmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UserInfoResponse {
    private Long id;
    private String userName;
    private Gender gender;
    private Double userHeight;
    private Double userWeight;
    private LocalDateTime lastConnect;
    private FitnessAbility fitnessAbility;
    private Integer dailyGoalMinute;

    private UserInfoResponse(UserInfoResponseBuilder builder) {
        this.id = builder.id;
        this.userName = builder.userName;
        this.gender = builder.gender;
        this.userHeight = builder.userHeight;
        this.userWeight = builder.userWeight;
        this.lastConnect = builder.lastConnect;
        this.fitnessAbility = builder.fitnessAbility;
        this.dailyGoalMinute = builder.dailyGoalMinute;
    }
    public static class UserInfoResponseBuilder implements CommonModelBuilder<UserInfoResponse> {

        private final Long id;
        private final String userName;
        private final Gender gender;
        private final Double userHeight;
        private final Double userWeight;
        private final LocalDateTime lastConnect;
        private final FitnessAbility fitnessAbility;
        private final Integer dailyGoalMinute;

        public UserInfoResponseBuilder(UserInfo userInfo) {
            this.id = userInfo.getId();
            this.userName = userInfo.getUserName();
            this.gender = userInfo.getGender();
            this.userHeight = userInfo.getUserHeight();
            this.userWeight = userInfo.getUserWeight();
            this.lastConnect = userInfo.getLastConnect();
            this.fitnessAbility = userInfo.getFitnessAbility();
            this.dailyGoalMinute = userInfo.getDailyGoalMinute();
        }

        @Override
        public UserInfoResponse build() {
            return new UserInfoResponse(this);
        }
    }
}
