package com.kyjg.fitnessassistmanager.model.userinfo;

import com.kyjg.fitnessassistmanager.enums.FitnessAbility;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class UserInfoUpdateRequest {
    @ApiModelProperty(value = "회원 몸무게(1~200)", required = true)
    @NotNull
    @Min(value = 1)
    @Max(value = 200)
    private Double userWeight;
    @ApiModelProperty(value = "운동 수행 능력", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private FitnessAbility fitnessAbility;
    @ApiModelProperty(value = "오늘의 목표치(0~600) / 분", required = true)
    @NotNull
    @Min(value = 0)
    @Max(value = 600)
    private Integer dailyGoalMinute;
}
