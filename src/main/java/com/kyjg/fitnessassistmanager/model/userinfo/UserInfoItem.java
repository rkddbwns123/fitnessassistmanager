package com.kyjg.fitnessassistmanager.model.userinfo;

import com.kyjg.fitnessassistmanager.entity.UserInfo;
import com.kyjg.fitnessassistmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.text.DecimalFormat;
import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UserInfoItem {
    private Long userInfoId;
    private String userName;
    private String gender;
    private String userHeight;
    private String userWeight;
    private String fitnessAbility;
    private Long bmiValue;
    private String obesityDegree;
    private LocalDate lastVisit;
    private String dailyGoal;

    private UserInfoItem(UserInfoItemBuilder builder) {
        this.userInfoId = builder.userInfoId;
        this.userName = builder.userName;
        this.gender = builder.gender;
        this.userHeight = builder.userHeight;
        this.userWeight = builder.userWeight;
        this.fitnessAbility = builder.fitnessAbility;
        this.bmiValue = builder.bmiValue;
        this.obesityDegree = builder.obesityDegree;
        this.lastVisit = builder.lastVisit;
        this.dailyGoal = builder.dailyGoal;

    }

    public static class UserInfoItemBuilder implements CommonModelBuilder<UserInfoItem> {

        private final Long userInfoId;
        private final String userName;
        private final String gender;
        private final String userHeight;
        private final String userWeight;
        private final String fitnessAbility;
        private final Long bmiValue;
        private final String obesityDegree;
        private final LocalDate lastVisit;
        private final String dailyGoal;

        public UserInfoItemBuilder(UserInfo userInfo) {
            this.userInfoId = userInfo.getId();
            this.userName = userInfo.getUserName();
            this.gender = userInfo.getGender().getGenderName();
            this.userHeight = userInfo.getUserHeight() + "cm";
            this.userWeight = userInfo.getUserWeight() + "kg";
            this.fitnessAbility = userInfo.getFitnessAbility().getAbilityName();
            this.bmiValue = Math.round((userInfo.getUserWeight() / (userInfo.getUserHeight() * userInfo.getUserHeight() / 10000)));

            String bmi = "저체중";
            if (this.bmiValue >= 24.9) {
                bmi = "비만";
            } else if (this.bmiValue >= 22.9) {
                bmi = "과제충";
            } else if (this.bmiValue >= 18.5) {
                bmi = "정상";
            }
            this.obesityDegree = bmi;
            this.lastVisit = LocalDate.now();
            this.dailyGoal = userInfo.getDailyGoalMinute() + "(분)";
        }

        @Override
        public UserInfoItem build() {
            return new UserInfoItem(this);
        }
    }
}
