package com.kyjg.fitnessassistmanager.model.userinfo;

import com.kyjg.fitnessassistmanager.enums.FitnessAbility;
import com.kyjg.fitnessassistmanager.enums.Gender;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class UserInfoRequest {
    @ApiModelProperty(value = "회원 이름(1~20자)", required = true)
    @NotNull
    @Length(min = 1, max = 20)
    private String userName;
    @ApiModelProperty(value = "성별", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Gender gender;
    @ApiModelProperty(value = "회원 키(0~250)", required = true)
    @NotNull
    @Min(value = 0)
    @Max(value = 250)
    private Double userHeight;
    @ApiModelProperty(value = "회원 몸무게(1~200)", required = true)
    @NotNull
    @Min(value = 1)
    @Max(value = 200)
    private Double userWeight;
    @ApiModelProperty(value = "운동 수행 능력", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private FitnessAbility fitnessAbility;
    @ApiModelProperty(value = "오늘의 목표치(0~600) / 분", required = true)
    @NotNull
    @Min(value = 0)
    @Max(value = 600)
    private Integer dailyGoalMinute;
}
