package com.kyjg.fitnessassistmanager.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
